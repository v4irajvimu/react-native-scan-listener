import React, { useEffect, useReducer } from "react";
import KeyEvent from "react-native-keyevent";

const initialState = {
  in_progress: false,
  completed: false,
  qrString: ""
};

function reducer(state, action) {
  switch (action.type) {
    case "increment": {
      return {
        in_progress: action.keyCode !== 20 ? true : false,
        completed: action.keyCode === 20,
        qrString: `${state.qrString}${
          action.keyCode === 59 || action.keyCode === 20
            ? ""
            : action.pressedKey
        }`
      };
    }
    case "reset": {
      return initialState;
    }
    default:
      throw new Error();
  }
}

const ScannerInputListener = ({
  children,
  OnComplete = null,
  OnKeyDown = null
}) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    KeyEvent.onKeyDownListener(keyEvent => {
      const keyCode = Number(keyEvent.keyCode);
      const pressedKey = keyEvent.pressedKey.toString();
      dispatch({ type: "increment", keyCode, pressedKey });
    });

    return () => {
      dispatch({ type: "reset" });
      KeyEvent.removeKeyDownListener();
    };
  }, []);

  useEffect(() => {
    const { in_progress, completed, qrString } = state;
    if (!in_progress && completed) {
      OnComplete && OnComplete(qrString);
      dispatch({ type: "reset" });
    }
    return () => {};
  }, [state]);

  return <>{children}</>;
};

export default ScannerInputListener;
